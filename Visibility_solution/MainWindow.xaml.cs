﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Visibility_solution;

namespace Visibility_solution
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Fields
        private Model terrainModel;
        private View terrainView;
        private Controller controller;
        private Point lastPosPan;
        private IDrawable _drawableCircle;
        private IDrawable _drawableCross;
        private readonly Pen _pen;
        private bool _circleIndicator;
        #endregion

        #region Handlers
        /// <summary>
        /// Creates open dialog for loading input heightmap file.
        /// </summary>
        private void openHeightMapFile(object sender, RoutedEventArgs e)
        {
            // Create OpenFileDialog 
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            // Display OpenFileDialog by calling ShowDialog method 
            Nullable<bool> result = dlg.ShowDialog();

            // Get the selected file name and display in a TextBox 
            if (result == true)
            {
                // Open document 
                string filename = dlg.FileName;

                // load Model and initialize view on it
                terrainModel.loadModel(filename);
                terrainView.initialize();
            }
        }

        /// <summary>
        /// Exit application handler for menuitem.
        /// </summary>
        private void exitApplication(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Handler for mouse wheel, manages zooming function.
        /// </summary>
        private void terrainViewMouseWheelHandler(object sender, MouseWheelEventArgs e)
        {
            // If the mouse wheel delta is positive, zoom in
            if (e.Delta > 0)
            {
                controller.zoomIn(e.Delta);
            }

            // If the mouse wheel delta is negative, zoom out
            if (e.Delta < 0)
            {
                controller.zoomOut(e.Delta);
            }
        }

        /// <summary>
        /// Handler for right mouse button down, pan function.
        /// </summary>
        private void terrainViewMouseRightButtonDownHandler(object sender, MouseButtonEventArgs e)
        {
            // pan function
            lastPosPan = e.GetPosition(terrainViewCanvas);
            terrainViewCanvas.CaptureMouse();
        }

        /// <summary>
        /// Handler for right mouse button up, pan function.
        /// </summary>
        private void terrainViewMouseRightButtonUpHandler(object sender, MouseButtonEventArgs e)
        {
            // pan function
            terrainViewCanvas.ReleaseMouseCapture();
        }

        /// <summary>
        /// Handler for mouse move on entire window.
        /// </summary>
        private void terrainViewMouseMoveHandler(object sender, MouseEventArgs e)
        {
            // code bellow is for canvas panning
            if (terrainViewCanvas.IsMouseCaptured)
            {
                Vector v = lastPosPan - e.GetPosition(terrainViewCanvas);
                controller.pan(v.X, v.Y);
                lastPosPan = e.GetPosition(terrainViewCanvas);
            }
        }

        /// <summary>
        /// Handler for mouse move on canvas, notifies controller about new cursor position.
        /// </summary>
        private void terrainViewCanvasMouseMoveHandler(object sender, MouseEventArgs e)
        {
            Point mousePos = e.GetPosition(terrainViewCanvas);
            
            controller.positionChanged(mousePos);

            // draw circle only after first left mouse button click
            if (!_circleIndicator && _drawableCircle != null)
            {
                // draw circle
                _pen.Draw(_drawableCircle, mousePos);
                // update circle radius property
                terrainView.circleRadius = Circle.getDistance(terrainView.circleCenter, mousePos);
            }
        }

        /// <summary>
        /// Hadler for left mouse button click - represent circle center and circle drawing
        /// </summary>
        private void terrainViewCanvasMouseLeftButtonDownHandler(object sender, MouseButtonEventArgs e)
        {
            // first left mouse click
            if (_circleIndicator)
            { 
                // if there is previous circle and cross, delete it from canvas
                if (_drawableCircle != null)
                    _pen.Erase(_drawableCircle);
                if (_drawableCross != null)
                    _pen.Erase(_drawableCross);

                Point mousePos = Mouse.GetPosition(terrainViewCanvas);

                // create new cross and circle
                _drawableCross = new Cross();
                _drawableCircle = new Circle(Mouse.GetPosition(terrainViewCanvas));

                // add cross and circle into canvas
                _pen.Down(_drawableCross);
                _pen.Down(_drawableCircle);

                // draw only center cross for now
                if (_drawableCross != null)
                    _pen.Draw(_drawableCross, mousePos);

                // update center point
                terrainView.circleCenter = mousePos;
                //double radius = getDistance(_center, location);
            }
            // second mouse click
            else if (!_circleIndicator)
            {
                Point mousePos = Mouse.GetPosition(terrainViewCanvas);

                // draw circle on second click
                if (_drawableCircle != null)
                   _pen.Draw(_drawableCircle, mousePos);

                // update radius
                terrainView.circleRadius = Circle.getDistance(terrainView.circleCenter, mousePos);

                HeightPointsLists lists = terrainModel.getHighestAndLowestPoints(terrainView.circleCenter, terrainView.circleRadius);

                terrainView.heighestPoints = lists.heighestPoints;
                terrainView.lowestPoints = lists.lowestPoints;

                this.highestPointsGrid.DataContext = terrainView.heighestPoints;
                this.lowestPointsGrid.DataContext = terrainView.lowestPoints;
            }
            _circleIndicator = !_circleIndicator;
        }

        /// <summary>
        /// Handler for previewing pressed key. Just for ignoring space.
        /// </summary>
        private void viewerOffsetTextBoxPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// Handler for previewing input in textbox. Tries parse input to double number.
        /// </summary>
        private void viewerOffsetTextBoxPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            //Create a string combining the text to be entered with what is already there.
            int cursorPos = this.viewerOffsetTextBox.CaretIndex;
            string nextText;
            if (cursorPos > 0)
            {
                nextText = this.viewerOffsetTextBox.Text.Substring(0, cursorPos) + e.Text + this.viewerOffsetTextBox.Text.Substring(cursorPos);
            }
            else
            {
                nextText = this.viewerOffsetTextBox.Text + e.Text;
            }

            double testVal;

            // test actual value if it is valid number
            if (!Double.TryParse(nextText, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture.NumberFormat, out testVal))
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// Handler for setting viewer offest property from textbox.
        /// </summary>
        private void buttonViewerOffsetClick(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(this.viewerOffsetTextBox.Text)) {
                // Update textBox binding property explicitly after clicking on Set button
                BindingExpression binding = this.viewerOffsetTextBox.GetBindingExpression(TextBox.TextProperty);
                binding.UpdateSource();
            }
        }
        #endregion

        /// <summary>
        /// Main window initializon, creates Model, View and Controller
        /// </summary>
        public MainWindow()
        {
            terrainModel = new Model();
            terrainView = new View();
            controller = new Controller();

            terrainView.setModel(terrainModel);
            controller.setModel(terrainModel);
            controller.setView(terrainView);

            InitializeComponent();
            _pen = new Pen(terrainViewCanvas);
            _circleIndicator = true;

            terrainView.setCanvas(this.terrainViewCanvas);

            // for test only.. load heightmap on startup
            //terrainModel.loadModel("../../data/heightmap_ASCII");
            //terrainView.initialize();

            // set data context for bindings
            this.DataContext = terrainView;
            //this.groupFrom.DataContext = terrainView;

            // register handlers to mouse move, wheel and buttons
            this.MouseWheel += new MouseWheelEventHandler(terrainViewMouseWheelHandler);
            this.MouseRightButtonUp += new MouseButtonEventHandler(terrainViewMouseRightButtonUpHandler);
            this.MouseRightButtonDown += new MouseButtonEventHandler(terrainViewMouseRightButtonDownHandler);
            this.MouseMove += new MouseEventHandler(terrainViewMouseMoveHandler);
            this.terrainViewCanvas.MouseMove += new MouseEventHandler(terrainViewCanvasMouseMoveHandler);
            this.terrainViewCanvas.MouseLeftButtonDown += new MouseButtonEventHandler(terrainViewCanvasMouseLeftButtonDownHandler);
        }
    }
}
