﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Visibility_solution
{
    /// <summary>
    /// Class representing circle drawing.
    /// </summary>
    class Circle : IDrawable
    {
        #region Fields
        private Point _center;
        private Point _circumference;
        #endregion

        #region Property
        public Ellipse circle { get; private set; }
        public Ellipse circumferencePoint { get; private set; }
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor for circle.
        /// Circle consist of circumference and small point on it.
        /// </summary>
        /// <param name="location">Center point of circle</param>
        public Circle(Point location)
        {
            circle = new Ellipse
            {
                Stroke = Brushes.Green,
                StrokeThickness = 1,
                //Margin = new Thickness(location.X, location.Y, 0, 0)
            };
            circumferencePoint = new Ellipse
            {
                Stroke = Brushes.Green,
                StrokeThickness = 1,
                //Margin = new Thickness(location.X, location.Y, 0, 0),
                Width = 6,
                Height = 6,
                Fill = Brushes.DarkGreen
            };
            _center = location;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Drawing circumference and small point on it.
        /// </summary>
        /// <param name="location">Center of circle.</param>
        public void Draw(Point location)
        {
            if (circle != null)
            {
                _circumference = location;
                double radius = getDistance(_center, location);

                circle.Margin = new Thickness(_center.X - radius, _center.Y - radius, 0, 0);

                circle.Width = 2 * radius;
                circle.Height = 2 * radius;
            }
            if (circumferencePoint != null)
            {
                circumferencePoint.Margin = new Thickness(location.X-3, location.Y-3, 0, 0);
            }
        }

        /// <summary>
        /// Calculate distance between two points in 2D cartesian system.
        /// </summary>
        /// <param name="point1">First point.</param>
        /// <param name="point2">Second point.</param>
        /// <returns>Distance between two points.</returns>
        public static double getDistance(Point point1, Point point2)
        {
            //pythagorean theorem c^2 = a^2 + b^2
            //thus c = square root(a^2 + b^2)
            double a = (double)(point2.X - point1.X);
            double b = (double)(point2.Y - point1.Y);

            return Math.Sqrt(a * a + b * b);
        }
        #endregion
    }
}
