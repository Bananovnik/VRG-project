﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Visibility_solution
{
    /// <summary>
    /// Class of height point storing its height and coordinates.
    /// Used only for showing top 10 highest and lowest points.
    /// </summary>
    class HeightPoint : IComparable<HeightPoint>
    {
        #region Properties
        public int height { get; set; }
        public Point coord { get; set; }
        #endregion

        #region Method
        public int CompareTo(HeightPoint other)
        {
            if (other.height > this.height)
                return -1;
            else if (other.height == this.height)
                return 0;
            else // (other.height < this.height)
                return 1;
        }
        #endregion

        #region Constructor
        public HeightPoint(int height, Point coord)
        {
            this.height = height;
            this.coord = coord;
        }
        #endregion
    }
}
