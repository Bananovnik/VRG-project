﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Visibility_solution
{
    /// <summary>
    /// Pen for drawing circle and cross on canvas.
    /// </summary>
    public class Pen
    {
        #region Field
        private readonly Canvas _holder;
        #endregion

        #region Methods
        /// <summary>
        /// Add object to canvas.
        /// </summary>
        /// <param name="obj">Object to be added.</param>
        public void Down(IDrawable obj)
        {
                //if more shapes are added:
                //better use a switch-statement
            if (obj is Circle)
            {
                _holder.Children.Add((obj as Circle).circle);
                _holder.Children.Add((obj as Circle).circumferencePoint);
            }
            if (obj is Cross)
            {
                _holder.Children.Add((obj as Cross).LineX);
                _holder.Children.Add((obj as Cross).LineY);
            }
        }

        /// <summary>
        /// Delete specific object from canvas
        /// </summary>
        /// <param name="obj">Object to be deleted.</param>
        public void Erase(IDrawable obj)
        {
            if (obj is Circle)
            {
                _holder.Children.Remove((obj as Circle).circle);
                _holder.Children.Remove((obj as Circle).circumferencePoint);
            }
            if (obj is Cross)
            {
                _holder.Children.Remove((obj as Cross).LineX);
                _holder.Children.Remove((obj as Cross).LineY);
            }
        }

        /// <summary>
        /// Draw object on specific point.
        /// </summary>
        /// <param name="obj">Object to be drawn</param>
        /// <param name="location">Location</param>
        public void Draw(IDrawable obj, Point location)
        {
            obj.Draw(location);
        }
        #endregion

        #region Constructor
        public Pen(Canvas holder)
        {
            _holder = holder;
        }
        #endregion
    }
}
