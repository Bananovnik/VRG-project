﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Visibility_solution
{
    /// <summary>
    /// Class representing cross drawing
    /// </summary>
    public class Cross : IDrawable
    {
        #region Properties
        public Line LineX { get; private set; }
        public Line LineY { get; private set; }
        #endregion

        #region Constructor
        public Cross()
        {
            LineX = new Line
            {
                Stroke = Brushes.Green,
                StrokeThickness = 1,
                X1 = 0,
                X2 = 0,
                Y1 = 0,
                Y2 = 0
            };

            LineY = new Line
            {
                Stroke = Brushes.Green,
                StrokeThickness = 1,
                X1 = 0,
                X2 = 0,
                Y1 = 0,
                Y2 = 0
            };
        }
        #endregion

        #region Methods
        public void Draw(Point location)
        {
            LineX.X1 = location.X-10;
            LineX.Y1 = location.Y;
            LineX.X2 = location.X+10;
            LineX.Y2 = location.Y;

            LineY.X1 = location.X;
            LineY.Y1 = location.Y-10;
            LineY.X2 = location.X;
            LineY.Y2 = location.Y+10;
        }
        #endregion
    }
}
