﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Visibility_solution
{
    public interface IDrawable
    {
        void Draw(Point location);
    }
}
