﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Media.Imaging;
using System.ComponentModel;

namespace Visibility_solution
{
    interface IView
    {
        void setCanvas(Canvas canvas);
        void setModel(Model model);
        void initialize();

        void zoomIn(int delta);
        void zoomOut(int delta);

        void pan(double deltaX, double deltaY);
    }

    /// <summary>
    /// Class, that represent View on our model.
    /// 
    /// </summary>
    class View : IView, INotifyPropertyChanged
    {
        #region Fields
        private Canvas _terrainViewCanvas;
        private Model _terrainModel;
        private Image _image;
        private Point _relativePosition;
        private Point _absolutePosition;
        private double _viewerOffset;
        private int _heightAtPos;
        private double _circleRadius;
        private Point _circleCenter;
        public List<HeightPoint> _heighestPoints;
        public List<HeightPoint> _lowestPoints;
        #endregion

        #region Properties
        public List<HeightPoint> heighestPoints
        {
            get { return _heighestPoints; }
            set
            {
                _heighestPoints = value;
                //OnPropertyChanged("heighestPoints");
            }
        }

        public List<HeightPoint> lowestPoints
        {
            get { return _lowestPoints; }
            set
            {
                _lowestPoints = value;
                //OnPropertyChanged("lowestPoints");
            }
        }

        public Point relativePosition
        {
            get { return _relativePosition; }
            set { _relativePosition = value;

                int x = (int)value.X, y = (int)value.Y;
                // do not access outside of array dimentions
                if (value.X >= _terrainModel.nrows)
                    x = _terrainModel.ncols;
                else if (value.X < 0)
                    x = 0;
                if (value.Y >= _terrainModel.ncols)
                    y = _terrainModel.nrows;
                else if (value.Y < 0)
                    y = 0;

                // set height property used in application
                heightAtPos = _terrainModel.getHeightAtPoint(x,y);
                OnPropertyChanged("relativePosition"); 
            }
        }

        public Point absolutePosition
        {
            //get { return (_actualRelativePosition.X).ToString("F2") + " " + (_actualRelativePosition.Y).ToString("F2"); }
            get { return _absolutePosition; }
            set { _absolutePosition = value;
                OnPropertyChanged("absolutePosition");
            }
        }

        public int heightAtPos
        {
            get { return _heightAtPos; }
            set { _heightAtPos = value;
                OnPropertyChanged("heightAtPos");
            }
        }

        public double viewerOffset
        {
            get { return _viewerOffset; }
            set { _viewerOffset = value; }
        }

        public double circleRadius
        {
            get { return _circleRadius; }
            set { _circleRadius = value;
                OnPropertyChanged("circleRadius");
            }
        }

        public Point circleCenter
        {
            get { return _circleCenter; }
            set { _circleCenter = value;
                OnPropertyChanged("circleCenter");
            }
        }
        #endregion

        #region Methods
        public void setCanvas(Canvas canvas)
        {
            _terrainViewCanvas = canvas;
        }

        public void setModel(Model model)
        {
            _terrainModel = model;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public void initialize()
        {
            int ncols = _terrainModel.ncols;
            int nrows = _terrainModel.nrows;

            // create image from out heightmap model
            _image = new Image();
            _image.Width = _terrainViewCanvas.Width = _terrainModel.ncols;
            _image.Height = _terrainViewCanvas.Height = _terrainModel.nrows;
            _image.Source = _terrainModel.heightMapImage;

            // just image flip and rotare correction
            _image.RenderTransformOrigin = new Point(0.5, 0.5);
            TransformGroup tg = new TransformGroup();
            ScaleTransform scaleTransform = new ScaleTransform();
            RotateTransform rotateTransform = new RotateTransform(-90);
            scaleTransform.ScaleX = 1;
            scaleTransform.ScaleY = -1;
            tg.Children.Add(rotateTransform);
            tg.Children.Add(scaleTransform);
            _image.RenderTransform = tg;

            _terrainViewCanvas.Children.Add(_image);

            _viewerOffset = 0.0;

            return;
        }

        /// <summary>
        /// Zoom in function for canvas.
        /// </summary>
        /// <param name="delta">Number of zoom increments.</param>
        public void zoomIn(int delta)
        {
            var s = (ScaleTransform)((TransformGroup)_terrainViewCanvas.RenderTransform).Children.First(st => st is ScaleTransform);
            s.ScaleX += 0.001 * delta;
            //s.ScaleY += 0.0001 * delta; // top left origin
            s.ScaleY -= 0.001 * delta; // bottom left origin
            /*if (s.ScaleX < 0.1 || s.ScaleY < 0.1)
                s.ScaleX = s.ScaleY = 0.1;*/
            if (s.ScaleX < 0.1)
                s.ScaleX = 0.1;
            if (s.ScaleY > -0.1)
                s.ScaleY = -0.1;
        }

        /// <summary>
        /// Zoom out function for canvas.
        /// </summary>
        /// <param name="delta">Number of zoom decrements.</param>
        public void zoomOut(int delta)
        {
            var s = (ScaleTransform)((TransformGroup)_terrainViewCanvas.RenderTransform).Children.First(st => st is ScaleTransform);
            s.ScaleX += 0.001 * delta;
            //s.ScaleY += 0.0001 * delta; // top left origin
            s.ScaleY -= 0.001 * delta; // bottom left origin
            if (s.ScaleX < 0.1)
                s.ScaleX = 0.1;
            if (s.ScaleY > -0.1)
                s.ScaleY = -0.1;
        }

        /// <summary>
        /// Pan function for canvas.
        /// </summary>
        public void pan(double deltaX, double deltaY)
        {
            var t = (TranslateTransform)((TransformGroup)_terrainViewCanvas.RenderTransform).Children.First(tr => tr is TranslateTransform);
            t.X -= deltaX;
            //t.Y -= deltaY; // top left origin
            t.Y += deltaY; // bottom left origin
        }
        #endregion


    }
}
