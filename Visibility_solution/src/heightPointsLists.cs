﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Visibility_solution
{
    /// <summary>
    /// Class just for returning two list as once.
    /// </summary>
    class HeightPointsLists
    {
        #region Properties
        public List<HeightPoint> heighestPoints { get; set; }
        public List<HeightPoint> lowestPoints { get; set; }
        #endregion

        #region Constructor
        public HeightPointsLists()
        {
            heighestPoints = new List<HeightPoint>();
            lowestPoints = new List<HeightPoint>();
        }
        #endregion
    }
}
