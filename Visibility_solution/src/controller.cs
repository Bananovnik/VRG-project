﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Visibility_solution
{
    interface IController
    {
        void zoomIn(int delta);
        void zoomOut(int delta);

        void pan(double deltaX, double deltaY);

        void positionChanged(Point newPos);

        void setModel(Model model);
        void setView(View view);
    }

    /// <summary>
    /// Controller class, processes the inputs captured in MainWindow handlers.
    /// </summary>
    class Controller : IController
    {
        #region Fields
        private Model terrainModel;
        private View terrainView;
        #endregion

        #region Properties

        #endregion

        public void zoomIn(int delta)
        {
            terrainView.zoomIn(delta);
        }

        public void zoomOut(int delta)
        {
            terrainView.zoomOut(delta);
        }

        public void pan(double deltaX, double deltaY)
        {
            terrainView.pan(deltaX, deltaY);
        }

        public void positionChanged(Point newPos)
        {
            terrainView.relativePosition = newPos;
            terrainView.absolutePosition = new Point(
                terrainModel.xllcorner + newPos.X * terrainModel.cellsize,
                terrainModel.yllcorner + newPos.Y * terrainModel.cellsize);
        }

        public void circleCenter(Point center)
        {
            terrainView.circleCenter = new Point(
                terrainModel.xllcorner + center.X * terrainModel.cellsize,
                terrainModel.yllcorner + center.Y * terrainModel.cellsize);
        }

        public void setModel(Model model)
        {
            terrainModel = model;
        }

        public void setView(View view)
        {
            terrainView = view;
        }
    }
}
