﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Visibility_solution
{
    interface IModel
    {
        int getHeightAtPoint(int x, int y);
        Point getAbsolutePointCoord(int x, int y);
        HeightPointsLists getHighestAndLowestPoints(Point center, double radius);
        void loadModel(string filename);
    }
    
    /// <summary>
    /// Class implementing Model of application - in this case HeightMap.
    /// It allows you to load heightmap from specific file.
    /// </summary>
    class Model : IModel
    {
        #region Fields
        private int _ncols;
        private int _nrows;
        private float _xllcorner;
        private float _yllcorner;
        private float _cellsize;
        private ushort[] _heightMap;
        
        #endregion

        #region Properties
        /// <summary>
        /// Bitmap image created from raw heights.
        /// </summary>
        public BitmapImage heightMapImage;

        public int ncols
        {
            get { return _ncols; }
        }

        public int nrows
        {
            get { return _nrows; }
        }

        public float xllcorner
        {
            get { return _xllcorner; }
        }

        public float yllcorner
        {
            get { return _yllcorner; }
        }

        public float cellsize
        {
            get { return _cellsize; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Loads new heightmap file and fill heightmap data into array.
        /// 
        /// </summary>
        /// <param name="filename">Heightmap filename to be loaded.</param>
        public void loadModel(string filename)
        {
            try
            {   // Open the text file using a stream reader.
                using (StreamReader reader = new StreamReader(filename))
                {
                    string line;
                    string[] items;
                    int rowCount = 0, colCount = 0;

                    // we need read the first five rows seperatly

                    line = reader.ReadLine(); // read first line - ncols
                    items = line.Split((char[])null, StringSplitOptions.RemoveEmptyEntries); // split by all whitespace

                    _ncols = int.Parse(items[1]); // ncols number

                    line = reader.ReadLine(); // read second line - nrows
                    items = line.Split((char[])null, StringSplitOptions.RemoveEmptyEntries);
                    _nrows = int.Parse(items[1]); // nrows number

                    line = reader.ReadLine(); // read third line - xllcorner
                    items = line.Split((char[])null, StringSplitOptions.RemoveEmptyEntries);
                    _xllcorner = float.Parse(items[1], CultureInfo.InvariantCulture.NumberFormat); // xllcorner number

                    line = reader.ReadLine(); // read fourth line - yllcorner
                    items = line.Split((char[])null, StringSplitOptions.RemoveEmptyEntries);
                    _yllcorner = float.Parse(items[1], CultureInfo.InvariantCulture.NumberFormat); // yllcorner number

                    line = reader.ReadLine(); // read fifth line - cellsize
                    items = line.Split((char[])null, StringSplitOptions.RemoveEmptyEntries);
                    _cellsize = float.Parse(items[1], CultureInfo.InvariantCulture.NumberFormat); // cellsize number

                    // allocate space for our heightmap
                    _heightMap = new ushort[nrows * ncols];

                    // read rest of file
                    while ((line = reader.ReadLine()) != null)
                    {
                        // split line by all whitespaces
                        items = line.Split((char[])null, StringSplitOptions.RemoveEmptyEntries);

                        colCount = 0;

                        foreach (string item in items)
                        {
                            UInt16 value = UInt16.Parse(item);

                            // add values
                            _heightMap[rowCount * ncols + colCount] = value;

                            colCount++;
                        }
                        rowCount++;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The file: " + filename + " could not be read!");
                Console.WriteLine(e.Message);
            }

            try
            {
                // create bitmap source
                BitmapSource heightMapSource = BitmapSource.Create(ncols, nrows, 96, 96, PixelFormats.Gray16, null, _heightMap, ncols * 2);

                // now convert source to image
                BmpBitmapEncoder encoder = new BmpBitmapEncoder();
                MemoryStream memoryStream = new MemoryStream();
                heightMapImage = new BitmapImage();

                // encode frame
                encoder.Frames.Add(BitmapFrame.Create(heightMapSource));
                encoder.Save(memoryStream);

                // save memory stream into bitmap image
                heightMapImage.BeginInit();
                heightMapImage.StreamSource = new MemoryStream(memoryStream.ToArray());
                heightMapImage.DecodePixelWidth = ncols;
                heightMapImage.EndInit();

                memoryStream.Close();

                // heightMapImage now contains image heightmap

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public int getHeightAtPoint(int x, int y)
        {
            if (ncols == 0 || nrows == 0)
                return 0;
            if (x >= nrows) x = nrows - 1;
            if (x < 0) x = 0;
            if (y >= ncols) y = ncols - 1;
            if (y < 0) y = 0;
            return _heightMap[x * ncols + y];
        }

        public Point getAbsolutePointCoord(int x, int y)
        {
            Point absoluteCoord = new Point();
            absoluteCoord.X = xllcorner + x * cellsize;
            absoluteCoord.Y = yllcorner + y * cellsize;
            return absoluteCoord;
        }

        /// <summary>
        /// Method for calculating first 10 lowest and highest points in circle on heightmap.
        /// There is optimalization for calculating: we only loop trough one quadrant of circle.
        /// The rest of the points in other quadrants are calculated symmetrically.
        /// </summary>
        /// <param name="center">Circle center coords</param>
        /// <param name="radius">Circle radius</param>
        /// <returns>Object, that contains two lists: lowest and highest points</returns>
        public HeightPointsLists getHighestAndLowestPoints(Point center, double radius)
        {
            HeightPointsLists lists = new HeightPointsLists();

            int xSym;
            int ySym;
            int counter = 0;
            
            for (int x = (int)center.X - (int)radius; x <= (int)center.X; x++)
            {
                for (int y = (int)center.Y - (int)radius; y <= (int)center.Y; y++)
                {
                    // only access points in circle
                    if ((x - (int)center.X) * (x - (int)center.X) + (y - (int)center.Y) * (y - (int)center.Y) <= radius * radius)
                    {
                        xSym = (int)center.X - (x - (int)center.X);
                        ySym = (int)center.Y - (y - (int)center.Y);
                        // (x, y), (x, ySym), (xSym , y), (xSym, ySym) are in the circle

                        List<HeightPoint> points = new List<HeightPoint>();

                        // check coords for out array bounds
                        bool xCoordCheck = (x >= 0 && x < ncols);
                        bool yCoordCheck = (y >= 0 && y < nrows);
                        bool xSymCoordCheck = (xSym >= 0 && xSym < ncols);
                        bool ySymCoordCheck = (ySym >= 0 && ySym < nrows);

                        // only add points that are in array bounds
                        if (xCoordCheck && yCoordCheck)
                        {
                            points.Add(new HeightPoint(getHeightAtPoint(x, y), new Point(x, y)));
                        }
                        if (xCoordCheck && ySymCoordCheck)
                        {
                            points.Add(new HeightPoint(getHeightAtPoint(x, ySym), new Point(x, ySym)));
                        }
                        if (xSymCoordCheck && yCoordCheck)
                        {
                            points.Add(new HeightPoint(getHeightAtPoint(xSym, y), new Point(xSym, y)));
                        }
                        if (xSymCoordCheck && ySymCoordCheck)
                        {
                            points.Add(new HeightPoint(getHeightAtPoint(xSym, xSym), new Point(xSym, ySym)));
                        }

                        // initialize lists with first 10 points
                        if (counter < 10)
                        {
                            lists.lowestPoints.AddRange(points);
                            lists.heighestPoints.AddRange(points);

                            lists.lowestPoints.Sort();
                            lists.heighestPoints.Sort();
                            lists.heighestPoints.Reverse();

                            // add number of new points to counter
                            counter += points.Count;

                            // if we added more points that needed to be added
                            if (counter > 10)
                            {
                                // remove redundant points
                                lists.lowestPoints.RemoveRange(10, counter - 10);
                                lists.heighestPoints.RemoveRange(10, counter - 10);
                            }
                        }

                        // now loop trough symetrical points to check and add to lowest/highest lists
                        foreach (HeightPoint point in points)
                        {
                            // check for lowest list
                            if (point.CompareTo(lists.lowestPoints.Last()) < 0) // (minPoint.height > pointI.height)
                            {
                                lists.lowestPoints.RemoveAt(lists.lowestPoints.Count - 1);
                                lists.lowestPoints.Add(point);

                                lists.lowestPoints.Sort();
                            }

                            // check for highest list
                            if (point.CompareTo(lists.heighestPoints.Last()) > 0) // (minPoint.height < pointI.height)
                            {
                                lists.heighestPoints.RemoveAt(lists.heighestPoints.Count - 1);
                                lists.heighestPoints.Add(point);

                                lists.heighestPoints.Sort();
                                lists.heighestPoints.Reverse();
                            }
                        }
                    }
                }
            }

            return lists;
        }
        #endregion

        #region Constructor
        public Model()
        {
            _ncols = _nrows = 0;
            _xllcorner = _yllcorner = _cellsize = 0.0f;
        }
        #endregion
    }
}
